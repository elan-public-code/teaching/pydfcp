# pyDFCP

## Description
This project contains Python implementations of generalised non-smooth Newton solvers and complementarity functions to solve the Discrete Frictional Contact Problem (DFCP).

## Author
Thibaut METIVET

## Dependencies
Python 3 code is provided and simply requires the additional `numpy` standard library.

## License
This code is licensed according to the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

